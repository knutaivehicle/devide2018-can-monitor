import qbs
import qbs.Process
import qbs.File
import qbs.FileInfo
import qbs.TextFile
import "../../../libs/openFrameworksCompiled/project/qtcreator/ofApp.qbs" as ofApp

Project{
    property string of_root: '../../..'

    ofApp {
        name: { return FileInfo.baseName(sourceDirectory) }

        files: [
          "src/ofApp.cpp",
          "src/ofApp.h",
          "src/kvaser.h",
        ]

        of.addons: []

        // additional flags for the project. the of module sets some
        // flags by default to add the core libraries, search paths...
        // this flags can be augmented through the following properties:

        // list of additional system pkgs to include
        of.pkgConfigs: []

        // include search paths
        of.includePaths: [
          "/opt/ros/kinetic/include"
        ]

        // flags passed to the c compiler
        of.cFlags: []

        // flags passed to the c++ compiler
        of.cxxFlags: []

        // flags passed to the linker
        of.linkerFlags: [
          "-L/opt/ros/kinetic/lib"
        ]

        // defines are passed as -D to the compiler
        // and can be checked with #ifdef or #if in the code
        of.defines: []

        // osx only, additional frameworks to link with the project
        of.frameworks: []

        // static libraries
        of.staticLibraries: []

        // dynamic libraries
        of.dynamicLibraries: [
          "roscpp",
          "rostime",
          "rosconsole",
          "roscpp_serialization"
        ]

        // other flags can be set through the cpp module: http://doc.qt.io/qbs/cpp-module.html
        // eg: this will enable ccache when compiling
        //
        // cpp.compilerWrapper: 'ccache'

        Depends{
            name: "cpp"
        }

        // common rules that parse the include search paths, core libraries...
        Depends{
            name: "of"
        }

        // dependency with the OF library
        Depends{
            name: "openFrameworks"
        }
    }

    property bool makeOF: true  // use makfiles to compile the OF library
    // will compile OF only once for all your projects
    // otherwise compiled per project with qbs


    property bool precompileOfMain: false  // precompile ofMain.h
    // faster to recompile when including ofMain.h
    // but might use a lot of space per project

    references: [FileInfo.joinPaths(of_root, "/libs/openFrameworksCompiled/project/qtcreator/openFrameworks.qbs")]
}
