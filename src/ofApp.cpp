#include "ofApp.h"
#include <boost/format.hpp>

void ofApp::setup() {

  ////openframeworks

  //setup in general
  ofSetFrameRate(50); // 50 fps == 20 ms

  //NOTE: we can 'set' background color, then it will be applied automatically at the start of 'draw()'
  ofSetBackgroundColor(100);

  //font
  ofTrueTypeFont::setGlobalDpi(72);
  txt.load("DejaVuSansMono.ttf", 10);

  ////ros

  //subscribers
  kvch1.initialize("/can0/can_tx", "/can0/can_rx"); //can_tx -> received msgs., can_rx -> trasmiting msgs. / strange naming.

}

void ofApp::exit() {

  // if main window is being closed, simply quit this app.
  ofExit();

}

void ofApp::update() {
  ;
}

void ofApp::draw() {

  ////2D visualization
  int linestep = 15;
  txt.drawString("CAN Messages", 20, linestep * 2);
  txt.drawString("  rxb size : " + std::to_string(kvch1.rxb.size()), 20, linestep * 3);
  txt.drawString(" ID : DLC : 0  1  2  3  4  5  6  7", 20, linestep * 4);
  for (int i = 0; i < kvch1.rxb.size(); i++) {
    can_msgs::Frame frm = kvch1.rxb[i];
    std::string buf = "";
    buf += str(boost::format("%03x") % static_cast<int>(frm.id)) + " :  " + std::to_string(frm.dlc) + "  : ";
    for (int c = 0; c < frm.dlc; c++) {
      buf += str(boost::format("%02x") % static_cast<int>(frm.data[c])) + " ";
    }
    txt.drawString(buf, 20, linestep * (5 + i));
  }
  kvch1.clear();
  //// end of '2D visualization'

  //ros task
  ros::spinOnce();
  // --> ros::spinOnce() will call all the callbacks waiting to be called at that point in time.
  // --> so, after this call, all data will be fully updated with most recent set of data, automatically.
}

void ofApp::keyPressed(int key)
{
  switch(toupper(key))
  {
  case 'A':
    // undef
    break;
  default:
    ;
  }
}

int main(int argc, char **argv){

  //ros - init
  ros::init(argc, argv, "ofviz_knut2018_can_bus_monitor");

  //of
#ifdef OF_APP_GRAPHICS

  // window settings obj.
  ofGLFWWindowSettings settings;

  // create window
  settings.setSize(300, 800);
  settings.setPosition(ofVec2f(0,0));
  settings.resizable = true;
  shared_ptr<ofAppBaseWindow> mainWindow = ofCreateWindow(settings);

  // create app.
  shared_ptr<ofApp> mainApp(new ofApp);

  // register the app. & start!
  ofRunApp(mainWindow, mainApp);
  ofRunMainLoop();

#else

  ofAppNoWindow nowindow;
  ofSetupOpenGL(&nowindow, 0, 0, OF_WINDOW);
  ofRunApp(new ofApp());

#endif
}
