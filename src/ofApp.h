#pragma once

//of
#include "ofMain.h"
#include "ofAppGLFWWindow.h"
#include "ofAppNoWindow.h"

//NOTE:comment out following to disable graphic windows completely. -> no window mode..
#define OF_APP_GRAPHICS

//ros i/f
#include "kvaser.h"

class ofApp : public ofBaseApp {
public:

  //kvaser
  ofviz::kvaser::channel kvch1;

  //of - main window
  void setup();
  void update();
  void draw();
  void exit();
  void keyPressed(int key);
  ofTrueTypeFont txt;

};
