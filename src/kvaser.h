#pragma once

//stl
#include <vector>

//ros
#include <ros/ros.h>
#include <can_msgs/Frame.h>

//
namespace ofviz {
namespace kvaser {

class channel {
public:

  //buffer
  std::vector<can_msgs::Frame> rxb;
  std::vector<can_msgs::Frame> txb;

  //ros i/f
  ros::NodeHandle n;
  std::string topic_sub;
  std::string topic_pub;
  ros::Subscriber sub;
  ros::Publisher pub;

  void callback(const can_msgs::Frame& inFr) {
    rxb.push_back(inFr);
  }

  void clear() {
    rxb.clear();
  }

  void initialize(std::string topic_sub_, std::string topic_pub_ = "") {
    topic_sub = topic_sub_;
    topic_pub = topic_pub_;
    //set subscriber
    sub = n.subscribe(topic_sub, 500, &channel::callback, this);
    //set publisher
    pub = n.advertise<can_msgs::Frame>(topic_pub, 500);
  }
};

} // end of 'namespace kvaser'
} // end of 'namespace ofviz'
